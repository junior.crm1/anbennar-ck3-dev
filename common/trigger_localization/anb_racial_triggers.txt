﻿### Culture ###

culture_conversion_racial_culture_requirement = {
	global = culture_conversion_racial_culture_requirement_global
	global_not = culture_conversion_racial_culture_requirement_global
	first = culture_conversion_racial_culture_requirement_global
	first_not = culture_conversion_racial_culture_requirement_global
}

is_illegitimate_race_trigger = {
	global = is_illegitimate_race_trigger_global
	global_not = is_illegitimate_race_trigger_global
	first = is_illegitimate_race_trigger_global
	first_not = is_illegitimate_race_trigger_global
}

is_legitimate_race_trigger = {
	global = is_legitimate_race_trigger_global
	global_not = is_legitimate_race_trigger_global
	first = is_legitimate_race_trigger_global
	first_not = is_legitimate_race_trigger_global
}

all_powerful_vassals_must_be_legitimate_trigger = {
	global = all_powerful_vassals_must_be_legitimate_trigger_global
	global_not = all_powerful_vassals_must_be_legitimate_trigger_global
	first = all_powerful_vassals_must_be_legitimate_trigger_global
	first_not = all_powerful_vassals_must_be_legitimate_trigger_global
}