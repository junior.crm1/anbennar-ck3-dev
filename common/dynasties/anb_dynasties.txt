﻿dynasty_silmuna = {
	name = "dynn_Silmuna"
	culture = damerian
}

dynasty_siloriel = {
	name = "dynn_Siloriel"
	culture = lorentish
}

dynasty_silgarion = {
	name = "dynn_Silgarion"
	culture = damerian
}

dynasty_silistra = {
	name = "dynn_Silistra"
	culture = damerian
}

dynasty_silurion = {
	name = "dynn_Silurion"
	culture = damerian
}

dynasty_silcalas = {
	name = "dynn_Silcalas"
	culture = damerian
}

dynasty_silebor = {
	name = "dynn_Silebor"
	culture = damerian
}

dynasty_silnara = {
	name = "dynn_Silnara"
	culture = damerian
}

dynasty_tretunis = {
	name = "dynn_Tretunis"
	culture = tretunic
}

dynasty_divenscourge = {
	name = "dynn_Divenscourge"
	culture = dalric
}

dynasty_roilsardis = {
	name = "dynn_Roilsardis"
	culture = roilsardi
}

dynasty_vis = {
	prefix = "dynnp_sil"
	name = "dynn_Vis"
	culture = hillfoot_halfling
	motto = dynn_Vis_motto
}

dynasty_derhilding = {
	name = "dynn_Derhilding"
	culture = olavish
}

dynasty_sorncost = {
	prefix = "dynnp_sil"
	name = "dynn_Sorncost"
	culture = sorncosti
}

dynasty_lorentis = {
	name = "dynn_Lorentis"
	culture = lorenti
}

dynasty_enteba = {
	name = "dynn_Enteba"
	culture = entebenic
}

dynasty_jaherzuir = {
	name = "dynn_Jaherzuir"
	culture = sun_elvish
}

dynasty_redstone = {
	name = "dynn_Redstone"
	culture = ruby_dwarvish
}

dynasty_uelaire = {
	prefix = "dynnp_of"
	name = "dynn_Uelaire"
	culture = "old_damerian"
}

dynasty_roysfort = {
	name = "dynn_Roysfort"
	culture = roysfoot_halfling
}

dynasty_iochand = {
	name = "dynn_Iochand"
	culture = creek_gnomish
}

dynasty_southroy = {
	name = "dynn_Southroy"
	culture = creek_gnomish
}

dynasty_nimsnoms = {
	name = "dynn_Nimsnoms"
	culture = creek_gnomish
}

dynasty_timekeeper = {
	name = "dynn_Timekeeper"
	culture = creek_gnomish
}

dynasty_bluddythol = {
	name = "dynn_Bluddythol"
	culture = iochander
}

dynasty_donnderward = {
	name = "dynn_Donnderward"
	culture = iochander
}

dynasty_hooifield = {
	name = "dynn_Hooifield"
	culture = iochander
}

dynasty_vernid = {
	name = "dynn_Vernid"
	culture = vernid
}

dynasty_plainsby = {
	name = "dynn_Plainsby"
	prefix = "dynnp_of"
	culture = vernid
}

dynasty_vinerick = {
	name = "dynn_Vinerick"
	prefix = "dynnp_of"
	culture = wexonard
}

dynasty_vinerick_old = {
	name = "dynn_Vinerick"
	prefix = "dynnp_of"
	culture = wexonard
}

dynasty_butcherson = {
	name = "dynn_Butcherson"
	culture = wexonard
}


dynasty_iacoban = {
	name = "dynn_Iacoban"
	culture = corvurian
}

dynasty_coddorran = {
	name = "dynn_Coddorran"
	culture = cliff_gnomish
}

dynasty_askeling = {
	name = "dynn_Askeling"
	culture = reverian
}

dynasty_gawe = {
	name = "dynn_Gawe"
	culture = gawedi
}

dynasty_mooring = {
	name = "dynn_Mooring"
	culture = moorman
}

dynasty_arthil = {
	name = "dynn_ta_arthil"
	culture = moon_elvish
}

dynasty_ta_galsheah = {
	name = "dynn_ta_galsheah"
	culture = moon_elvish
}

dynasty_adshaw = {
	name = "dynn_Adshaw"
	culture = old_alenic
}

dynasty_serpentsgard = {
	name = "dynn_Serpentsgard"
	culture = blue_reachman
}

dynasty_cobbler = {
	name = "dynn_Cobbler"
	culture = blue_reachman
}

dynasty_wex = {
	name = "dynn_Wex"
	culture = wexonard
}

dynasty_sugambic = {
	name = "dynn_Sugambic"
	culture = wexonard
}

dynasty_bisan = {
	name = "dynn_Bisan"
	culture = wexonard
}

dynasty_silverhammer = {
	name = "dynn_Silverhammer"
	culture = silver_dwarvish
}

dynasty_esmar = {
	name = "dynn_Esmar"
	culture = esmari
}

dynasty_bennon = {
	name = "dynn_Bennon"
	culture = esmari
}

dynasty_estallen = {
	prefix = "dynnp_sil"
	name = "dynn_Estallen"
	culture = ryalani
}

dynasty_ryalan = {
	name = "dynn_Ryalan"
	culture = ryalani
}

dynasty_leslinpar = {
	prefix = "dynnp_sil"
	name = "dynn_Leslinpar"
	culture = esmari
}

dynasty_havoran = {
	name = "dynn_Havoran"
	culture = esmari
}

dynasty_lunetein = {
	name = "dynn_talunetein"
	culture = moon_elvish
}

dynasty_farran = {
	name = "dynn_Farran"
	culture = esmari
}

dynasty_eldman = {
	name = "dynn_Eldman"
	culture = crownsman
}

dynasty_vrorensson = {
	name = "dynn_Vrorensson"
	culture = white_reachman
}

dynasty_ebonfrost = {
	name = "dynn_Ebonfrost"
	culture = black_castanorian
}

dynasty_aldwoud = {
	name = "dynn_Aldwoud"
	culture = white_reachman
}

dynasty_tederfremh = {
	name = "dynn_Tederfremh"
	culture = moon_elvish
}

dynasty_cast = {
	prefix = "dynnp_sil"
	name = "dynn_Cast"
	culture = castanorian
}

dynasty_anor = {
	prefix = "dynnp_sil"
	name = "dynn_Anor"
	culture = castanorian
}

dynasty_marr = {
	name = "dynn_Marr"
	culture = marrodic
}

dynasty_balmire = {
	name = "dynn_Balmire"
	culture = old_alenic
}

dynasty_minesmiter = {
	name = "dynn_Minesmiter"
	culture = copper_dwarvish
}

dynasty_locke = {
	name = "dynn_Locke"
	culture = oakfoot_halfling
	motto = dynn_Locke_motto
}

dynasty_akalses = {
	prefix = "dynnp_szel"
	name = "dynn_Akalses"
	culture = zanite
}

dynasty_erubas = {
	prefix = "dynnp_szel"
	name = "dynn_Erubas"
	culture = zanite
}

dynasty_esshyl = {
	name = "dynn_Esshyl"
	culture = castanorian
}

dynasty_verteben = {
	prefix = "dynnp_of"
	name = "dynn_Verteben"
	culture = crownsman
	motto = dynn_Verteben_motto
}

dynasty_shieldrest = {
	prefix = "dynnp_of"
	name = "dynn_Shieldrest"
	culture = adeanic
	motto = dynn_Shieldrest_motto
}

dynasty_asrat = {
	name = "dynn_Asrat"
	culture = surani
}

dynasty_semiz = {
	name = "dynn_Semiz"
	culture = surani
}

dynasty_segh = {
	prefix = "dynnp_az"
	name = "dynn_Segh"
	culture = citrine_dwarvish
}

dynasty_gelkalis = {
	prefix = "dynnp_szel"
	name = "dynn_Gelkalis"
	culture = gelkar
}

dynasty_maqet = {
	prefix = "dynnp_szal"
	name = "dynn_Maqet"
	culture = maqeti
}

dynasty_veraesa = {
	name = "dynn_Varaesa"
	culture = moon_elvish
}

dynasty_escin = {
	prefix = "dynnp_of"
	name = "dynn_Escin"
	culture = adeanic
}

dynasty_panoutid = {
	name = "dynn_Panoutid"
	culture = kheteratan
}

dynasty_shenotid = {
	name = "dynn_Shenotid"
	culture = kheteratan
}

dynasty_sokkanid = {
	name = "dynn_Sokkanid"
	culture = kheteratan
}

dynasty_crodamis = {
	name = "dynn_Crodamis"
	culture = kheteratan
}

dynasty_crodamos = {
	name = "dynn_Crodamos"
	culture = kheteratan
}

dynasty_deshakos = {
	name = "dynn_Deshakos"
	culture = deshaki
}

dynasty_keskhasa = {
	name = "dynn_Keskhasa"
	culture = khasani
}

dynasty_oldhaven = {
	prefix = "dynnp_of"
	name = "dynn_Oldhaven"
	culture = marcher
}

dynasty_aubergentis = {
	name = "dynn_Aubergentis"
	culture = lorenti
}

dynasty_sidericis = {
	name = "dynn_Sidericis"
	culture = damerian
}

dynasty_acromis = {
	name = "dynn_Acromis"
	culture = old_damerian
}

dynasty_prumuris = {
	name = "dynn_Prumuris"
	culture = old_damerian
}

dynasty_cymlan = {
	name = "dynn_Cymlan"
	culture = moon_elvish
}

dynasty_magehand = {	#Serondal Magehand
	name = "dynn_Magehand"
	culture = moon_elvish
}

dynasty_blacktower = {	#Serondal Magehand
	name = "dynn_Blacktower"
	culture = vertesker
}

dynasty_longlance = {	#of Caylen Longlance fame
	name = "dynn_Longlance"
	culture = adeanic
}

dynasty_drakesford = {
	name = "dynn_Drakesford"
	culture = gawedi
}

dynasty_oudescker = {
	name = "dynn_Oudescker"
	culture = gawedi
}

dynasty_gladeguard = {
	name = "dynn_Gladeguard"
	culture = moon_elvish
}

dynasty_plumwall = {
	name = "dynn_Plumwall"
	culture = old_damerian
}

dynasty_cliffman = {
	name = "dynn_Cliffman"
	culture = lenco_damerian
}

dynasty_taxwick = {
	name = "dynn_Taxwick"
	culture = old_damerian
}

dynasty_exwes = {
	prefix = "dynnp_of"
	name = "dynn_Exwes"
	culture = exwesser
}

dynasty_sarfort = {
	name = "dynn_Sarfort"
	culture = castanorian
	motto = dynn_Sarfort_motto
}

dynasty_karnid = {
	name = "dynn_Karnid"
	culture = korbarid
	motto = dynn_Karnid_motto
}

dynasty_aldanid = {
	name = "dynn_Aldanid"
	culture = korbarid
	motto = dynn_aldanid_motto
}

dynasty_eskrumbnid = {
	name = "dynn_Eskrumbnid"
	culture = korbarid
	motto = dynn_eskrumbnid_motto
}

dynasty_buzasnid = {
	name = "dynn_Buzasnid"
	culture = korbarid
	motto = dynn_buzasnid_motto
}

dynasty_denarzuir = {
	name = "dynn_Denarzuir"
	culture = sun_elvish
	motto = dynn_Denarzuir_motto
}

dynasty_tiferben = {
	name = "dynn_Tiferben"
	culture = corvurian
	motto = dynn_Tiferben_motto
}

dynasty_fiachlar = {
	prefix = "dynnp_sil"
	name = "dynn_Fiachlar"
	culture = corvurian
	motto = dynn_sil_Fiachlar_motto
}

dynasty_magda = {
	prefix = "dynnp_sil"
	name = "dynn_Magda"
	culture = wexonard
}

dynasty_aelvar = {
	prefix = "dynnp_sil"
	name = "dynn_Aelvar"
	culture = derannic
	motto = dynn_sil_Aelvar_motto
}

dynasty_colson = {
	name = "dynn_Colson"
	culture = derannic
	motto = dynn_sil_Aelvar_motto
}

dynasty_ording = {
	prefix = "dynnp_of"
	name = "dynn_Ording"
	culture = lorenti
	motto = dynn_of_Ording_motto
}

dynasty_ventis = {
	name = "dynn_ventis"
	culture = lorenti
	motto = dynn_Ventis_motto
}

dynasty_duskwatcher = {
	name = "dynn_Duskwatcher"
	culture = moon_elvish
	motto = dynn_Duskwatcher_motto
}

dynasty_caylenoris = {
	name = "dynn_Caylenoris"
	culture = lorenti
}

dynasty_nisabat = {
	name = "dynn_Nisabat"
	culture = bahari
}

dynasty_casthil = {
	prefix = "dynnp_sil"
	name = "dynn_Casthil"
	culture = lorentish
	motto = dynn_sil_Casthil_motto
}

dynasty_balgard = {
	name = "dynn_balgard"
	culture = old_alenic
}

dynasty_kobali = {
	name = "dynn_Kobali"
	culture = milcori
	# motto = dynn_Kobali_motto
}

dynasty_aldegarde = {
	name = "dynn_Aldegarde"
	culture = lorentish
	motto = dynn_Aldegarde_motto
}

dynasty_arthelis = {
	name = "dynn_Arthelis"
	culture = lorenti
}

dynasty_brannis = {
	name = "dynn_Brannis"
	culture = lorenti
}

dynasty_medrontis = {
	name = "dynn_Medrontis"
	culture = lorenti
}

dynasty_calonis = {
	name = "dynn_Calonis"
	culture = lorenti
}

dynasty_gwevoris = {
	name = "dynn_Gwevoris"
	culture = lorenti
}

dynasty_derancestir = {
	prefix = "dynnp_sil"
	name = "dynn_Derancestir"
	culture = damerian
}

dynasty_luantis = {
	name = "dynn_Luantis"
	culture = lorenti
}

dynasty_amburtis = {
	name = "dynn_Amburtis"
	culture = lorenti
}

dynasty_clobronh = {
	name = "dynn_Clobronh"
	culture = lorenti
}

dynasty_sigvardsson = {
	name = "dynn_sigvardsson"
	culture = dalric
}

dynasty_goldeneyes = {
	name = "dynn_goldeneyes"
	culture = old_alenic
}

dynasty_frostwall = {
	name = "dynn_frostwall"
	culture = old_alenic
}

dynasty_bjarnsson = {
	name = "dynn_bjarnsson"
	culture = dalric
}

dynasty_sidaett = {
	name = "dynn_sidaett"
	culture = dalric
}

dynasty_fuglborg = {
	name = "dynn_fuglborg"
	culture = dalric
}

dynasty_carantis = {
	name = "dynn_carantis"
	culture = lorentish	
}

dynasty_garmonis = {
	name = "dynn_Garmonis"
	culture = lorenti	
}

dynasty_harascilde = {
	prefix = "dynnp_of"
	name = "dynn_Harascilde"
	culture = lorentish	
}

dynasty_drostening = {
	name = "dynn_Drostening"
	culture = black_castanorian	
}

dynasty_ottocam = {
	name = "dynn_Ottocam"
	culture = wexonard	
}

dynasty_singkeep = {
	name = "dynn_Singkeep"
	culture = moon_elvish
}

dynasty_marelis = {
	name = "dynn_Marelis"
	culture = esmari
}

dynasty_nurael = {
	name = "dynn_nurael"
	culture = moon_elvish	
}

dynasty_larthan = {
	name = "dynn_larthan"
	culture = moon_elvish	
}

dynasty_silcarod = {
	name = "dynn_silcarod"
	culture = moon_elvish	
}

dynasty_seawatcher = {
	name = "dynn_seawatcher"
	culture = moon_elvish
}

dynasty_truesight = {
	name = "dynn_truesight"
	culture = moon_elvish
}

dynasty_toarnen = {
	name = "dynn_toarnen"
	culture = roilsardi
}

dynasty_cronesford = {
	name = "dynn_cronesford"
	culture = milcori
}

dynasty_celliande = {
	name = "dynn_celliande"
	culture = roilsardi
}

dynasty_arannen = {
	prefix = "dynnp_sil"
	name = "dynn_arannen"
	culture = roilsardi
}

dynasty_endersby = {
	name = "dynn_endersby"
	culture = milcori
}

dynasty_wesdam = {
	prefix = "dynnp_sil"
	name = "dynn_wesdam"
	culture = lenco_damerian
}

dynasty_wispsiren = {
	name = "dynn_wispsiren"
	culture = milcori
}

dynasty_nurcedor = {
	name = "dynn_nurcedor"
	culture = businori
}

dynasty_wayguard = {
	name = "dynn_wayguard"
	culture = marcher
}

dynasty_beldarbronhd = {
	name = "dynn_beldarbronhd"
	culture = businori
}

dynasty_devaced = {
	name = "dynn_devaced"
	culture = marcher
}

dynasty_gabalaire = {
	name = "dynn_gabalaire"
	culture = damerian
}

dynasty_acengard = {
	name = "dynn_acengard"
	culture = adeanic
	prefix = "dynnp_of"
}

dynasty_aldenmore = {
	prefix = "dynnp_of"
	name = "dynn_aldenmore"
	culture = castanorian
}

dynasty_ryalfeld = {
	prefix = "dynnp_of"
	name = "dynn_ryalfeld"
	culture = castanorian
}

dynasty_sheldgard = {
	prefix = "dynnp_of"
	name = "dynn_sheldgard"
	culture = castanorian
}

dynasty_eswall = {
	prefix = "dynnp_of"
	name = "dynn_eswall"
	culture = castanorian
}

dynasty_cestir = {
	prefix = "dynnp_sil"
	name = "dynn_cestir"
	culture = damerian
	motto = dynn_Cestir_motto
}

dynasty_cannleis = {
	name = "dynn_cannleis"
	culture = old_damerian
}

dynasty_teagansfield = {
	prefix = "dynnp_of"
	name = "dynn_teagansfield"
	culture = crownsman
	motto = dynn_Teagansfield_motto
}

dynasty_bronzewing = {
	prefix = "dynnp_of"
	name = "dynn_Bronzewing"
	culture = vernid
	motto = dynn_Bronzewing_motto
}

dynasty_tails_end = {
	prefix = "dynnp_of"
	name = "dynn_Tails_End"
	culture = vernid
}

dynasty_gagliardid = {
	name = "dynn_Gagliardid"
	culture = vernid
}

dynasty_laudaris = {
	name = "dynn_Laudaris"
	culture = vernid
}

dynasty_walterid = {
	name = "dynn_Walterid"
	culture = vernid
}

dynasty_andhol = {
	prefix = "dynnp_of"
	name = "dynn_andhol"
	culture = old_esmari
}

dynasty_hagstow = {
	prefix = "dynnp_of"
	name = "dynn_hagstow"
	culture = castanorian
}

dynasty_sarelzuir = {
	name = "dynn_sarelzuir"
	culture = sun_elvish
}

dynasty_olorzuir = {
	name = "dynn_olorzuir"
	culture = sun_elvish
}

dynasty_vulzinzuir = {
	name = "dynn_vulzinzuir"
	culture = sun_elvish
}

dynasty_amarienzuir = {
	name = "dynn_amarienzuir"
	culture = sun_elvish
}

dynasty_alvazuir = {
	name = "dynn_alvazuir"
	culture = sun_elvish
}

dynasty_dalzuir = {
	name = "dynn_dalzuir"
	culture = sun_elvish
}

dynasty_varamzuir = {
	name = "dynn_varamzuir"
	culture = sun_elvish
}

dynasty_irrliazuir = {
	name = "dynn_irrliazuir"
	culture = sun_elvish
}

dynasty_uyelzuir = {
	name = "dynn_uyelzuir"
	culture = sun_elvish
}

dynasty_lezuir = {
	name = "dynn_lezuir"
	culture = sun_elvish
}

dynasty_aralzuir = {
	name = "dynn_aralzuir"
	culture = sun_elvish
}

dynasty_nestezuir = {
	name = "dynn_nestezuir"
	culture = sun_elvish
}

dynasty_tirenzuir = {
	name = "dynn_tirenzuir"
	culture = sun_elvish
}

dynasty_barseen = {
	prefix = "dynnp_szel"
	name = "dynn_barseen"
	culture = barsibu
}

dynasty_brasan = {
	prefix = "dynnp_szel"
	name = "dynn_brasan"
	culture = brasanni
}

dynasty_cockerwall = {
	name = "dynn_cockerwall"
	culture = moorman
}

dynasty_oldport = {
	prefix = "dynnp_of"
	name = "dynn_Oldport"
	culture = lorenti
}

dynasty_threeflowers = {
	prefix = "dynnp_of"
	name = "dynn_Threeflowers"
	culture = lorenti
}

dynasty_highcour = {
	prefix = "dynnp_of"
	name = "dynn_Highcour"
	culture = lorenti
}

dynasty_crownscour = {
	name = "dynn_Crownscour"
	culture = lorenti
}

dynasty_lasean = {
	prefix = "dynnp_sil"
	name = "dynn_Lasean"
	culture = lorentish
	motto = dynn_Lasean_motto
}

dynasty_hookfield = {
	prefix = "dynnp_of"
	name = "dynn_hookfield"
	culture = exwesser
}

dynasty_baldfather = {
	name = "dynn_Baldfather"
	culture = gawedi
}

dynasty_adenica = {
	prefix = "dynnp_of"
	name = "dynn_adenica"
	culture = adeanic
}

dynasty_casnaview = {
	name = "dynn_casnaview"
	culture = lorenti
}

dynasty_mireleigh = {
	prefix = "dynnp_of"
	name = "dynn_mireleigh"
	culture = adeanic
}

dynasty_falseharbour = {
	prefix = "dynnp_of"
	name = "dynn_falseharbour"
	culture = adeanic
}

dynasty_taranton = {
	prefix = "dynnp_of"
	name = "dynn_taranton"
	culture = adeanic
}

dynasty_acenaire = {
	prefix = "dynnp_of"
	name = "dynn_acenaire"
	culture = adeanic
}

dynasty_eastbow = {
	name = "dynn_eastbow"
	culture = old_damerian
}

dynasty_cantercurse = {
	prefix = "dynnp_of"
	name = "dynn_cantercurse"
	culture = adeanic
}

dynasty_castlebridge = {
	prefix = "dynnp_of"
	name = "dynn_castlebridge"
	culture = crownsman
}

dynasty_thunderward = {
	prefix = "dynnp_of"
	name = "dynn_thunderward"
	culture = lorenti
}

dynasty_rohibon = {
	prefix = "dynnp_of"
	name = "dynn_rohibon"
	culture = adeanic
}

dynasty_shoreseeker = {
	name = "dynn_shoreseeker"
	culture = moon_elvish
}

dynasty_haightbobben = {
	prefix = "dynnp_of"
	name = "dynn_Haightbobben"
	culture = black_castanorian
}

dynasty_natharis = {
	name = "dynn_natharis"
	culture = damerian
}

dynasty_adad = {
	prefix = "dynnp_szal"
	name = "dynn_Adad"
	culture = masnsih
	motto = dynn_Adad_motto
}

dynasty_ardeth = {
	prefix = "dynnp_szal"
	name = "dynn_Ardeth"
	culture = masnsih
	motto = dynn_Ardeth_motto
}

dynasty_attalu = {
	prefix = "dynnp_szal"
	name = "dynn_Attalu"
	culture = masnsih
	motto = dynn_Attalu_motto
}

dynasty_ayarzil = {
	prefix = "dynnp_szal"
	name = "dynn_Ayarzil"
	culture = masnsih
	motto = dynn_Ayarzil_motto
}

dynasty_surubaz = {
	prefix = "dynnp_szal"
	name = "dynn_Surubaz"
	culture = masnsih
	motto = dynn_Surubaz_motto
}

dynasty_yazkur = {
	prefix = "dynnp_szal"
	name = "dynn_Yazkur"
	culture = masnsih
	motto = dynn_Yazkur_motto
}

dynasty_zaid = {
	prefix = "dynnp_szal"
	name = "dynn_Zaid"
	culture = masnsih
	motto = dynn_Zaid_motto
}

dynasty_bulati = {
	prefix = "dynnp_szal"
	name = "dynn_Bulati"
	culture = masnsih
	motto = dynn_Bulati_motto
}

dynasty_selabis = {
	prefix = "dynnp_szal"
	name = "dynn_Selabis"
	culture = masnsih
	motto = dynn_Selabis_motto
}

dynasty_rijascar = {
	prefix = "dynnp_szal"
	name = "dynn_Rijascar"
	culture = masnsih
	motto = dynn_Rijascar_motto
}

dynasty_susi = {
	prefix = "dynnp_szal"
	name = "dynn_Susi"
	culture = masnsih
	motto = dynn_Susi_motto
}

dynasty_aruru = {
	prefix = "dynnp_szal"
	name = "dynn_Aruru"
	culture = masnsih
	motto = dynn_Aruru_motto
}

dynasty_betikalbu = {
	prefix = "dynnp_szal"
	name = "dynn_Betikalbu"
	culture = masnsih
	motto = dynn_Betikalbu_motto
}

dynasty_gerwick ={
	name = "dynn_Gerwick"
	culture = gawedi
}

dynasty_haightbobben = {
	prefix = "dynnp_of"
	name = "dynn_haightbobben"
	culture = black_castanorian
}

dynasty_natharis = {
	name = "dynn_natharis"
	culture = damerian
}

dynasty_marromarck = {
	prefix = "dynnp_of"
	name = "dynn_marromarck"
	culture = castanorian
}

dynasty_vihteriz = {
	prefix = "dynnp_az"
	name = "dynn_vihteriz"
	culture = stone_dwarvish
}

dynasty_mascabir = {
	prefix = "dynnp_mascabir"
	name = "dynn_mascabir"
	culture = castanite
}

dynasty_elentis = { 
	prefix = "dynnp_of"
	name = "dynn_elentis"
	culture = old_damerian
}

dynasty_dhaneir = { 
	prefix = "dynnp_sil"
	name = "dynn_dhaneir"
	culture = damerian
}

dynasty_dryadsvale = {
	prefix = "dynnp_of"
	name = "dynn_dryadsvale"
	culture = marrodic
}

dynasty_wendshaw = {
	prefix = "dynnp_of"
	name = "dynn_wendshaw"
	culture = marrodic
}

dynasty_griffonsquarrel = {
	prefix = "dynnp_of"
	name = "dynn_griffonsquarrel"
	culture = marrodic
}

dynasty_willowmore = {
	prefix = "dynnp_of"
	name = "dynn_willowmore"
	culture = marrodic
}

dynasty_hornwood = {
	prefix = "dynnp_of"
	name = "dynn_hornwood"
	culture = marrodic
}

dynasty_tailfeather = { 
	prefix = "dynpp_tailfeather"
	name = "dynn_tailfeather"
	culture = marrodic
}

dynasty_ginnfield = { 
	prefix = "dynpp_of"
	name = "dynn_ginnfield"
	culture = gawedi
}
	
dynasty_alenath = { 
	prefix = "dynnp_sil"
	name = "dynn_alenath"
	culture = gawedi
}
	
dynasty_westpier = { 
	prefix = "dynpp_of"
	name = "dynn_westpier"
	culture = gawedi
}

dynasty_giantswood = { 
   prefix = "dynpp_of"
   name = "dynn_giantswood"
   culture = "castanorian"
}

dynasty_moonbeam = { 
	prefix = "dynnp_sil"
	name = "dynn_moonbeam"
	culture = moon_elvish
	motto = dynn_moonbeam_motto
}

dynasty_gallopsway = { 
	prefix = "dynpp_of"
	name = "dynn_gallopsway"
	culture = adeanic
}

dynasty_humbercroft = {
	name = "dynn_Humbercroft"
	culture = gawedi
}
dynasty_drachongard = { 
   prefix = "dynpp_of"
   name = "dynn_drachongard"
   culture = "castanorian"
}

dynasty_khugsarod = {
   prefix = "az-"
   name = "dynn_khugsarod"
   culture = "agate_dwarvish"
}

dynasty_honeyaxe = {
   prefix = "dynpp_honeyaxe"
   name = "dynn_honeyaxe"
   culture = "oakfoot_halfling"
}

dynasty_trialwoud = { 
   prefix = "dynpp_of"
   name = "dynn_trialwoud"
   culture = "castanorian"
}

dynasty_shatterwood = { 
   prefix = "dynpp_of"
   name = "dynn_shatterwood"
   culture = "castanorian"
}

dynasty_bradesford = { 
   prefix = "dynpp_of"
   name = "dynn_bradesford"
   culture = "castanorian"
}

dynasty_serpentsmarck = { 
   prefix = "dynpp_of"
   name = "dynn_serpentsmarck"
   culture = "black_castanorian"
}

dynasty_hvitrvol = { 
   prefix = "dynpp_of"
   name = "dynn_hvitrvol"
   culture = "black_castanorian"
}

dynasty_nortmount = { 
   prefix = "dynpp_of"
   name = "dynn_nortmount"
   culture = "castanorian"
}


dynasty_ansson = {
	name = "dynn_Ansson"
	culture = black_castanorian
}

dynasty_deruwris = {
	name = "dynn_deruwris"
	culture = carnetori
}

dynasty_woodslover = {
	name = "dynn_woodslover"
	culture = moon_elvish
}
dynasty_uinteios = {
	name = "dynn_uinteios"
	culture = sormanni
}

dynasty_sorboduos = {
	name = "dynn_sorboduos"
	culture = sormanni
}

dynasty_ebenweal = {
	name = "dynn_ebenweal"
	culture = castanorian
}

dynasty_treoarcen = {
	name = "dynn_treoarcen"
	culture = marcher
}

dynasty_treun = {
	prefix = "dynnp_sil_na"
	name = "dynn_treun"
	culture = vernman
}

dynasty_losnaris = {
	name = "dynn_losnaris"
	culture = old_damerian
}

dynasty_taoris = {
	name = "dynn_taoris"
	culture = old_damerian
}

dynasty_laturis = {
	name = "dynn_laturis"
	culture = old_damerian
}

dynasty_aremornios = {
	name = "dynn_aremornios"
	culture = sormanni
}

dynasty_dunteris = {
	name = "dynn_dunteris"
	culture = damerian
}

dynasty_steedspear = {
	name = "dynn_Steedspear"
	culture = derannic
}

dynasty_fjorhavn = {
	name = "dynn_Fjorhavn"
	culture = derannic
}

dynasty_luminares = {
	name = "dynn_luminares"
	culture = tefori
}

dynasty_urzhana = {
	name = "dynn_urzhana"
	culture = bahari
}

dynasty_azaraha = {
	name = "dynn_azaraha"
	culture = brasanni
}

dynasty_khasani = {
	name = "dynn_khasani"
	culture = khasani
}

dynasty_nirhaunes = {
	name = "dynn_nirhaunes"
	culture = west_divenori
}

dynasty_orstengard = {
	prefix = "dynnp_av"
	name = "dynn_orstengard"
	culture = olavish
}

dynasty_visunid = {
	name = "dynn_visunid"
	culture = korbarid
}

dynasty_hortoselez = {
	name = "dynn_hortoselez"
	culture = businori
}

dynasty_taulez = {
	name = "dynn_taulez"
	culture = businori
}

dynasty_waasheshi = {
	name = "dynn_waasheshi"
	culture = kheteratan
}

dynasty_mocard = {
	name = "dynn_Mocard"
	culture = derannic
}

dynasty_leafdancer = {
	name = "dynn_Leafdancer"
	culture = moon_elvish
}

dynasty_scalecloak = {
	name = "dynn_scalecloak"
	culture = gawedi
}

dynasty_floodmark = {
	name = "dynn_floodmark"
	culture = gawedi
}

dynasty_robacard = {
	name = "dynn_Robacard"
	culture = wexonard
	motto = dynn_robacard_motto
}

dynasty_carstenard = {
	name = "dynn_Carstenard"
	culture = wexonard
}

dynasty_wincenard = {
	name = "dynn_Wincenard"
	culture = wexonard
}