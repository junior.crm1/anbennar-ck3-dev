﻿k_wex = {
	1000.1.1 = { change_development_level = 8 }
}

d_wexhills = {
	1015.6.7 = {
		holder = 65 #Dyren Wex
	}
}

c_wexkeep = {
	1000.1.1 = { change_development_level = 12 }
	1015.6.7 = {
		holder = 65 #Dyren Wex
	}
}

c_threeguard = {
	1000.1.1 = { change_development_level = 8 }
	1015.6.7 = {
		holder = 65 #Dyren Wex
	}
}

c_ironhill = {
	470.1.1 = {
		liege = d_wexhills
	}
	1000.1.1 = {
		change_development_level = 8
	}
	1020.1.1 = {
		holder = robacard_0001 # Hobert Robacard
	}
}

c_wexhills = {
	470.1.1 = {
		liege = d_wexhills
	}
	940.1.1 = { 
		holder = carstenard_0002
	}

	1006.4.23 = { 	#Richard Carstenard inherits it after his father dies
		holder = carstenard_0001
	}
}

d_ottocam = {
	1015.6.7 = {
		holder = 65 #Dyren Wex
	}
}

c_ottocam={
	960.1.1 = {
		holder = ottocam_0001 #Ottrac Ottocam
	}
	978.3.18 = {# placeholder
		holder = 0
	}
	
	994.1.1 = {
		holder = vinerick_0001
	}
}

c_vinerick = {
	1000.1.1 = { change_development_level = 9 }

	970.1.1 = {
		holder = vinerick_0004
	}

	990.1.1 = {
		holder = 0
	}
	
	994.1.1 = {
		holder = vinerick_0001
	}
}

c_indlebury = {
	1000.1.1 = { change_development_level = 9 }
	978.4.27 = { 	#Elrac the Butcher takes it in Butcher's Field Massacre or shortly after
		holder = butcherson_0002
	}

	1006.4.23 = { 	#Carstenards take it
		holder = carstenard_0001
		liege = d_wexhills
	}
}

d_bisan = {
	1005.9.2 = {
		holder = 67 #Hargen Bisan
	}
}

c_bisan = {
	1000.1.1 = { change_development_level = 9 }
	1005.9.2 = {
		holder = 67 #Hargen Bisan
	}
}

c_countsbridge = {
	957.2.16 = { 	
		holder = wincenard_0003
		liege = d_bisan
	}

	978.4.27 = { 	#Butcher's Field Massacre
		holder = wincenard_0002
	}

	1014.5.14 = { 	#Battle of Morban Flats bro
		holder = wincenard_0001
	}
}

c_corseton = {
	1008.4.9 = {
		holder = halfling_0001 # High Priest Folcard
	}
}

c_magdalaire = {
	1000.1.1 = { change_development_level = 9 }
	1015.3.21 = {
		holder = 551 #Magda sil Magda
	}
}

d_greater_bardswood = {
	900.1.1 = {
		change_development_level = 7
	}
	1005.9.2 = {
		holder = 67 #Hargen Bisan
	}
}

c_butchersfield = {
	978.4.27 = { 	#Elrac the Butcher takes it in Butcher's Field Massacre
		holder = butcherson_0002
	}

	1006.4.23 = { 	#Elrac's son inherits after Elrac dies in Relief of Wexkeep
		holder = butcherson_0001	#Roderick Butcherson
	}
}

c_bardswood = {
	1000.1.1 = { change_development_level = 7 }
}

c_stumpwood = {
	1000.1.1 = { change_development_level = 7 }
	978.4.27 = { 	#Elrac the Butcher takes it in Butcher's Field Massacre
		holder = butcherson_0002
	}

	1006.4.23 = { 	#Elrac's son inherits after Elrac dies in Relief of Wexkeep
		holder = butcherson_0001	#Roderick Butcherson
	}
}

d_sugamber = {
	970.3.4 = {
		holder = 66 #Rycroft Sugambic
	}
}

c_rhinmond = {
	1000.1.1 = { change_development_level = 9 }
}

d_escandar = {
	1000.1.1 = { change_development_level = 6 }
	1017.1.1 = {
		holder = 551 #Magda asil Magda
	}
}

c_escandar = {
	1015.3.21 = {
		holder = 551 #Magda sil Magda
	}
}

c_gnollsgate = {
	1015.3.21 = {
		holder = 551 #Magda sil Magda
	}
}

c_rupellion = {
	1015.3.21 = {
		holder = 551 #Magda sil Magda
	}
}

b_aelthil = {
	1015.3.21 = {
		holder = 552 #Celarion, Magda's elven husband
	}
}

b_amberflow = {
	1015.6.7 = {
		holder = moonbeam0001 #Barry, 63
	}
}